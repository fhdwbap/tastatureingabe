import java.io.*;

/**
 * Sehr elementares Beispiel einer Tastatureingabe.
 * Dabei wird auch das Konzept der Ausnahmebehandlungen (exceptions)
 * verwendet (siehe try/catch/finally).
 * @author FHDWBAP
 *
 */
public class Eingabe 
{

	public static void main(String[] args) 
	{
		System.out.println("Demo einer Tastatureingabe.");
		System.out.println("Ende durch Eingabe von (nur) [Return].");
		
		BufferedReader tastaturInput = new BufferedReader(new InputStreamReader(System.in)); 
	    String eingabe = ""; 
	     
        try 
        { 
        	while (true)
        	{
        		System.out.print("> ");
        		eingabe = tastaturInput.readLine(); 
        		if (eingabe.equals("")) 
        		{ 
        			System.out.println("Eingabe-Schleife wird beendet!"); 
        			break; 
        		}
                System.out.println(eingabe);
            } // end while 
        } catch (IOException ioe) 
        { 
        	System.out.println("Exception ausgeloest!");
            ioe.printStackTrace(); 
        } finally 
        { 
        	System.out.println("finally-Block erreicht.");
            try 
            { 
                if (tastaturInput != null)
                {
                    tastaturInput.close();
                    System.out.println("tastaturInput geschlossen.");
                }
            } catch (IOException ioe) 
            {
            	ioe.printStackTrace();
            } 
        }  // end try-catch-finally-Block
        System.out.println("eingabe: " + eingabe);

	} // end main()

} // end class Eingabe
